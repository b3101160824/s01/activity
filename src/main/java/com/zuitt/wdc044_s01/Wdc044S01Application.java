package com.zuitt.wdc044_s01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import  org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
// The main goal of a RestController is to handle incoming HTTP requests and generate HTTP responses.
// Annotations are optional. They are primarily used to enhance behavior, configuration, or processing of code elements
public class Wdc044S01Application {

	public static void main(String[] args) {
		SpringApplication.run(Wdc044S01Application.class, args);
	}

	@GetMapping("/hello")
	public String hello(@RequestParam(value="name", defaultValue="World") String name) {
		return  String.format("Hello %s!",name);
	}

	@GetMapping("/hi")
	public String hi(@RequestParam(value="user", defaultValue="Universe") String user) {
		return  String.format("Hello %s!",user);
	}
}
