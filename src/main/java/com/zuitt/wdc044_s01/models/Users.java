package com.zuitt.wdc044_s01.models;

import org.apache.catalina.User;

import javax.persistence.*;

@Entity
@Table(name="users")
public class Users {
    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String Username;

    @Column
    private String Password;

    public Users(){}

    public Users(String Username, String Password) {
        this.Username = Username;
        this.Password = Password;
    }

    public String getUsername() {
        return Username;
    }

    public void setUsername(String Username) {
        this.Username = Username;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }
}
