package com.zuitt.wdc044_s01.models.repositories;

import com.zuitt.wdc044_s01.models.Post;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostRepository extends CrudRepository <Post, Object>{

}
