package com.zuitt.wdc044_s01.models.repositories;

import com.zuitt.wdc044_s01.models.Users;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

public interface UserRepository extends CrudRepository <Users, Object>{

}
